<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Usuario extends Authenticatable implements JWTSubject
{
    use Notifiable;
    
    protected $table = 'tf_usuarios';
     
    protected $fillable = [
        'nome', 'login', 'senha', 'perfil'
    ];
 
    protected $hidden = [
        'senha'
    ];

    public function getJWTIdentifier()
    {
        return $this->id;
    }

    public function getJWTCustomClaims()
    {
        return [
            
            'login'      => $this->login,            
        ];
    }
}
