<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Usuario;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if (!is_null($request['login']) && $request['senha']) {

            $dados = $request->only('login', 'senha');

            $usuario = Usuario::where('login', $dados['login'])->first();
            
            if (Auth::check() || ($usuario && Hash::check($dados['senha'], $usuario->senha))) {
                
                $token = Auth::guard('api')->login($usuario);

                $dadosUsuario = [
                    'id'     => $usuario->id,
                    'login'  => $usuario->login,
                    'nome'   => $usuario->nome,
                    'perfil' => $usuario->perfil,
                ];           
                
                return $this->responseToken($token, $dadosUsuario);

            }

        }
    }


    /**
     * Método responsável por retornar o token gerado
     */
    private function responseToken($token, $dadosUsuario)
    {

        if ($token) {

            return response()->json([
                'token' => $token,
                'dadosUsuario' => $dadosUsuario,
            ]);

        } else {
            return response()->json([
                'error' => \Lang::get('auth.failed')
            ], 400);
        }

    }

    public function listaUsuarios()
    {
        $usuarios = Usuario::all();

        return json_encode($usuarios);
    }

    public function postUsuario(Request $request)
    {
        dd($request);
    }
}
